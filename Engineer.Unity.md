# Unity Coding Challenge

As part of your interview process with Circadence, we would like you to complete a sample coding project. You have 5 days to create a Unity application called *Web Query*. We will schedule an online code-review meeting with you after 5 days to discuss and review your code.

## Description

- Create a Unity/WebGL UX application that can query data from a remote source.
- Include UI elements for selecting the Search Area, query input Search Fields, a Search Button and a scrollable Results display text area.
- Support query on the following Search Areas: Users and Posts.
- Deserialize query results into JSON format before displaying.

## Requirements

1. Current version of [Unity](https://unity3d.com/get-unity/download/archive)
2. Use APIs on [JSONPlaceHolder](https://jsonplaceholder.typicode.com/) for queries
3. Use Unity [TMPro](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.1/api/TMPro.html) for all UI

## Bonus Points

You will get bonus points for:

- including the ability to ignore certificate errors
- including the ability to also query on Albums and then display their photo when selected

## Unity Q&A

- What are some of the key approaches to optimization that you have found in Unity?

- Event handling : Do you prefer editor wiring or code wiring?

- Scriptable Objects: have you used them, and if so how?

- Have you ever needed to write plugins for Unity? If so what was the purpose?

- Any experience with editor tooling? If so what was the purpose?

- Ever written any shaders? If so what was the purpose?

- Do you have experience targeting WebGL in Unity? If so, what were some of the challenges you encountered?

- Have you used assetbundles previously? If so, describe the architecture/flow and how it benefitted your project.

- Are you on any Unity forum sites? If so, which and what are your usernames?

- What is one change you would make to Unity if you could?

- Where do you find yourself needing Unity Coroutines most? Give examples of several times you've needed them and why.

- Describe a feature implementation that you were proud of or worked well.
